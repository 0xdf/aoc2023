#!/usr/bin/env python3

import sys


def badhash(s: str) -> int:
    h = 0
    for c in s:
        h += ord(c)
        h *= 17
        h %= 256
    return h


with open(sys.argv[1], "r") as f:
    words = f.read().strip().split(",")

part1 = sum(badhash(word) for word in words)
print(f"Part 1: {part1}")

boxes = [[] for _ in range(256)]
focal_len_map = {}
for inst in words:
    if "=" in inst:
        label, value = inst.split("=")
        if label not in boxes[badhash(label)]:
            boxes[badhash(label)].append(label)
        focal_len_map[label] = int(value)
    elif inst.endswith("-"):
        label = inst.strip("-")
        if label in boxes[badhash(label)]:
            boxes[badhash(label)].remove(label)
    else:
        assert False

part2 = 0
for bn, box in enumerate(boxes, 1):
    for ln, lens in enumerate(box, 1):
        part2 += bn * ln * focal_len_map[lens]

print(f"Part 2: {part2}")
