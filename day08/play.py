#!/usr/bin/env python3

import sys


with open(sys.argv[1], "r") as f:
    directions_str, _, *nodes = (l.strip() for l in f.readlines())

directions = [int(c == "R") for c in directions_str]
node_map = {}
for line in nodes:
    # AAA = (BBB, CCC)
    key, targets = line.split(" = ")
    node_map[key] = targets.strip("()").split(", ")

part1 = 0
len_dirs = len(directions)
zzzs = [0]
pos = "QJA"
while len(zzzs) < 10:
    dir = directions[part1 % len_dirs]
    pos = node_map[pos][dir]
    part1 += 1
    if pos.endswith("Z"):
        print(part1, part1 - zzzs[-1])
        zzzs.append(part1)

print(f"Part 1: {part1}")

part2 = ""
print(f"Part 2: {part2}")
