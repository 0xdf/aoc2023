#!/usr/bin/env python3

import sys
from typing import List


class Grid:
    def __init__(self, g: tuple[str]) -> None:
        self.grid = g
        self.rotate_counter_clockwise()

    def rotate_counter_clockwise(self) -> None:
        self.grid = tuple(map("".join, zip(*self.grid)))[::-1]

    def rotate_clockwise(self) -> None:
        for _ in range(3):
            self.rotate_counter_clockwise()

    def print_grid(self) -> None:
        print("\n".join(self.grid))
        print()

    def tilt_north(self) -> None:
        self.grid = tuple(
            "#".join(["".join(sorted(s, reverse=True)) for s in row.split("#")])
            for row in self.grid
        )

    def tilt_cycle(self) -> None:
        for _ in range(4):
            self.tilt_north()
            self.rotate_clockwise()

    def tilt_cycles(self, i) -> None:
        states = [self.grid]
        count = 0
        while count < i:
            self.tilt_cycle()
            count += 1
            if self.grid in states:
                break
            states.append(self.grid)
        if count == i:
            return
        first_seen = states.index(self.grid)
        cycle = count - first_seen
        self.grid = states[(i - first_seen) % cycle + first_seen]

    def total_load(self) -> int:
        # total = 0
        # len_row = len(self.grid[0])
        # for row in self.grid:
        #     for i, ch in enumerate(row):
        #         if ch == "O":
        #             total += len_row - i
        total = sum(
            len(self.grid[0]) - i
            for row in self.grid
            for i, ch in enumerate(row)
            if ch == "O"
        )
        return total


with open(sys.argv[1], "r") as f:
    grid_lines = tuple(l.strip() for l in f.readlines())

grid = Grid(grid_lines)
grid.tilt_north()
part1 = grid.total_load()
print(f"Part 1: {part1}")

grid2 = Grid(grid_lines)
grid2.tilt_cycles(1000000000)
part2 = grid2.total_load()
print(f"Part 2: {part2}")
