#!/usr/bin/env python3

import sys
import heapq


with open(sys.argv[1], "r") as f:
    lines = f.readlines()

grid = {
    (r, c): int(v) for r, line in enumerate(lines) for c, v in enumerate(line.strip())
}
height = len(lines)
width = len(lines[0].strip())

queue = [(0, 0, 0, 0, 0, 0)]
seen = set()

while queue:
    heat, r, c, dr, dc, s = heapq.heappop(queue)

    if (r, c, dr, dc, s) in seen:
        continue

    if r == height - 1 and c == width - 1:
        part1 = heat
        break

    seen.add((r, c, dr, dc, s))

    for next_dr, next_dc in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
        next_r = r + next_dr
        next_c = c + next_dc
        if next_r < 0 or next_r >= height or next_c < 0 or next_c >= width:
            continue  # off the grid, not valid
        if next_dr == -dr and next_dc == -dc:
            continue  # can't go backwards
        if next_dr == dr and next_dc == dc:
            if s < 3:
                heapq.heappush(
                    queue,
                    (
                        heat + grid[(next_r, next_c)],
                        next_r,
                        next_c,
                        next_dr,
                        next_dc,
                        s + 1,
                    ),
                )
            else:
                continue
        else:
            heapq.heappush(
                queue,
                (heat + grid[(next_r, next_c)], next_r, next_c, next_dr, next_dc, 1),
            )

print(f"Part 1: {part1}")

queue2 = [(0, 0, 0, 0, 0, 0)]
seen2 = set()

while queue2:
    heat, r, c, dr, dc, s = heapq.heappop(queue2)
    print(heat, r, c, dr, dc, s)

    if (r, c, dr, dc, s) in seen2:
        continue

    if r == height - 1 and c == width - 1:
        part2 = heat
        break

    seen2.add((r, c, dr, dc, s))

    for next_dr, next_dc in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
        next_r = r + next_dr
        next_c = c + next_dc
        if next_r < 0 or next_r >= height or next_c < 0 or next_c >= width:
            continue  # off the grid, not valid
        if next_dr == -dr and next_dc == -dc:
            continue  # can't go backwards
        if next_dr == dr and next_dc == dc:
            if s < 10:
                heapq.heappush(
                    queue2,
                    (
                        heat + grid[(next_r, next_c)],
                        next_r,
                        next_c,
                        next_dr,
                        next_dc,
                        s + 1,
                    ),
                )
            else:
                continue
        elif s >= 4 or (dr == 0 and dc == 0):
            heapq.heappush(
                queue2,
                (heat + grid[(next_r, next_c)], next_r, next_c, next_dr, next_dc, 1),
            )

print(f"Part 2: {part2}")
