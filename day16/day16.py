#!/usr/bin/env python3

import sys
from collections import deque


def count_powered(r: int, c: int, dr: int, dc: int) -> int:
    seen = set()
    queue = deque([(r, c, dr, dc)])

    while queue:
        r, c, dr, dc = queue.popleft()

        # move
        r += dr
        c += dc

        if not (0 <= r < height and 0 <= c < width):
            continue

        # turn
        next_dirs = []
        match grid[r][c]:
            case "/":
                # 0, 1 <-> -1, 0   1, 0 <-> 0, -1
                next_dirs.append((-dc, -dr))
            case "\\":
                # 0, 1 <-> 1, 0    0, -1 <-> -1, 0
                next_dirs.append((dc, dr))
            case "|":
                if dc == 0:
                    next_dirs.append((dr, dc))
                else:
                    next_dirs.extend([(1, 0), (-1, 0)])
            case "-":
                if dr == 0:
                    next_dirs.append((dr, dc))
                else:
                    next_dirs.extend([(0, 1), (0, -1)])
            case ".":
                next_dirs.append((dr, dc))
            case _:
                assert False, f"unknown char {grid[r][c]}"

        for dr, dc in next_dirs:
            if (r, c, dr, dc) not in seen:
                seen.add((r, c, dr, dc))
                queue.append((r, c, dr, dc))

    return len(set((r, c) for r, c, _, _ in seen))


with open(sys.argv[1], "r") as f:
    grid = [l.strip() for l in f.readlines()]
    height = len(grid)
    width = len(grid[0])

part1 = count_powered(0, -1, 0, 1)
print(f"Part 1: {part1}")

part2 = 0
for r in range(height):
    part2 = max(part2, count_powered(r, -1, 0, 1))
    part2 = max(part2, count_powered(r, width, 0, -1))
for c in range(width):
    part2 = max(part2, count_powered(-1, c, 1, 0))
    part2 = max(part2, count_powered(height, c, -1, 0))

print(f"Part 2: {part2}")
