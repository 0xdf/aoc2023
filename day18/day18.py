#!/usr/bin/env python3

import sys


def perimeter(points):
    total = 0
    for (r1, c1), (r2, c2) in zip(points, points[1:] + [points[0]]):
        total += abs(r1 - r2) + abs(c1 - c2)
    return total


def internal_area(points):
    # https://en.wikipedia.org/wiki/Shoelace_formula
    total = 0
    for (r1, c1), (r2, c2) in zip(points, points[1:] + [points[0]]):
        total += (r1 + r2) * (c1 - c2)
    return total // 2


dir_map = {"R": (0, 1), "L": (0, -1), "U": (-1, 0), "D": (1, 0)}

with open(sys.argv[1], "r") as f:
    lines = f.readlines()

points = [(0, 0)]
r, c = 0, 0
for line in lines:
    d, n, _ = line.split()
    dr, dc = dir_map[d]
    r += dr * int(n)
    c += dc * int(n)
    points.append((r, c))

# https://en.wikipedia.org/wiki/Pick%27s_theorem
# A = i + b/2 -1
# i = A - b/2 + 1
# total points = i + b = A + b/2 + 1
part1 = internal_area(points) + perimeter(points) // 2 + 1
print(f"Part 1: {part1}")

points2 = [(0, 0)]
r, c = 0, 0
for line in lines:
    _, _, hex_str = line.split()
    hex_str = hex_str.strip("#()")
    n = int(hex_str[:5], 16)
    dr, dc = dir_map["RDLU"[int(hex_str[5])]]
    r += dr * int(n)
    c += dc * int(n)
    points2.append((r, c))

part2 = internal_area(points2) + perimeter(points2) // 2 + 1
print(f"Part 2: {part2}")
